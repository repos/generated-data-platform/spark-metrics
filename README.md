This is a work in progress fork of https://github.com/banzaicloud/spark-metrics/ modified to meet WMF conventions.

## Packages

Jars are published to the project's `gitlab-maven` registry. 

### Installation
To install the current `SNAPSHOT` package, use the following dependency.
```
<dependency>
  <groupId>com.banzaicloud</groupId>
  <artifactId>spark-metrics_2.11</artifactId>
  <version>2.4.5-1.0.0-SNAPSHOT-wmf</version>
</dependency>
```

### Maven registry
To enable the registry, you'll need the following settings in your `pom.xml` (or equivalent).
```
<repositories>
  <repository>
    <id>gitlab-maven</id>
    <url>https://gitlab.wikimedia.org/api/v4/projects/197/packages/maven</url>
  </repository>
</repositories>

<distributionManagement>
  <repository>
    <id>gitlab-maven</id>
    <url>https://gitlab.wikimedia.org/api/v4/projects/197/packages/maven</url>
  </repository>

  <snapshotRepository>
    <id>gitlab-maven</id>
    <url>https://gitlab.wikimedia.org/api/v4/projects/197/packages/maven</url>
  </snapshotRepository>
</distributionManagement>
```
For more details and installation instructions see the registry homepage at https://gitlab.wikimedia.org/repos/generated-data-platform/spark-metrics/-/packages/75.

# Apache Spark metrics extensions

This is a repository for ApacheSpark metrics related custom classes (e.g. sources, sinks). We were trying to extend the Spark Metrics subsystem with a Prometheus sink but the [PR](https://github.com/apache/spark/pull/19775#issuecomment-371504349) was not merged upstream. In order to support others to use Prometheus we have externalized the sink and made available through this repository, thus there is no need to build an Apache Spark fork.

* [Prometheus sink](https://github.com/banzaicloud/spark-metrics/blob/master/PrometheusSink.md)

For further information how we use this extension and the Prometheus sink at [Banzai Cloud](https://banzaicloud.com/) please read these posts:

* [Monitoring Apache Spark with Prometheus](https://banzaicloud.com/blog/spark-monitoring/)<br/>
* [Monitoring multiple federated clusters with Prometheus - the secure way](https://banzaicloud.com/blog/prometheus-federation/)<br/>
* [Application monitoring with Prometheus and Pipeline](https://banzaicloud.com/blog/prometheus-application-monitoring/)<br/>


